<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>api_horario</title>
	 <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="css/index.min.css">
</head>
<body>
	  <div class="row" style="margin: 20px">
	  <br><br><br><br><br>
      <div class="col s12 z-depth-3 ">
      
  		<h4 class="center-align">KONECTA</h4>
	
      </div>
      <br> <br> <br> <br> <br>
      
      <div class="z-depth-2">
      
      	<table>
        <thead>
          <tr>
              <th>METHOD</th>
              <th>URL</th>
              <th>DESCRIPTION</th>
          </tr>
        </thead>

        <tbody>
          <tr>
            <td>POST</td>
            <td>dominio.example/api_horario/hora</td>
            <td>Retorna la hora actual desde el servidor</td>
          </tr>
        </tbody>
      </table>
      
      </div>
    </div>
</body>
</html>