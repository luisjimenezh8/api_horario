package com.konecta.api_horario.services;

import java.util.Calendar;

public class Time {
	
	private int year;
	private int month;
    private int day;
    private int hour;
    private int minutes;
    private int seconds;
    
    public void capturedTime(){
    	
    	Calendar calendario = Calendar.getInstance();
    	this.year = calendario.get(Calendar.YEAR);
	    this.hour = calendario.get(Calendar.HOUR_OF_DAY);
	    this.minutes = calendario.get(Calendar.MINUTE);
	    this.day = calendario.get(Calendar.DAY_OF_MONTH);
	    this.month = calendario.get(Calendar.MONTH) + 1;
	    this.seconds = calendario.get(Calendar.SECOND);
    }
    
    public int getDay(){
        return this.day;
    }
    
    public int getHour() {
        return this.hour;
    }
    
    public int getMinutes() {
        return this.minutes;
    }
    
    public int getYear() {
    	return this.year;
    }
    
    public int getMonth() {
    	return this.month;
    }
    
    public int getSeconds() {
    	return this.seconds;
    }
    
}
